/**
 * @file
 * @brief Implements OS-specific crash_handler_t methods using the Windows API.
 *
 * Finishes implementing crash_handler_t for Windows targets, using debugging
 * functions supplied in the Windows API.
 *
 * Note that Windows stack traces require WinXP SP2, WinS2K3 SP1 or higher.
 * The test for stack trace availability is done at run time.
**/

#include "AppHdr.h"

#include "crash.h"

#include "state.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

// Detect if this is a Windows target for stack tracing purposes
// (Cygwin counts as Windows in this case)
#if !defined(CRASH_DISABLE_OS_IMPLEMENTATION) \
    && (defined(TARGET_OS_WINDOWS) || defined(TARGET_COMPILER_CYGWIN)) \
    || defined(DOXYGEN_ONLY)

/**
 * @brief If defined, provides a Windows API crash_handler_t implementation.
 *
 * If not defined, this file's, the implementation will not be compiled.
 */
# define CRASH_WIN_ENABLED (1)

// Determine if DbgHelp types need to be defined for this target
// "Modern" WinSDK (5.2+) and mingw-w64 have these, but modern mingw32 does not
# if defined(__MINGW32__) && !defined(__MINGW64_VERSION_MAJOR)
// mingw32 -- dbghelp is only included in ver >= 4.1
#  if defined(__MINGW32_MAJOR_VERSION) \
    && (__MINGW32_MAJOR_VERSION < 4 \
        || (__MINGW32_MAJOR_VERSION == 4 && __MINGW32_MINOR_VERSION < 1)) \
        || defined(DOXYGEN_ONLY)
/**
 * @brief If defined, provide definitions for DbgHelp types.
 */
#   define CRASH_WIN_NEED_DBGHELP_TYPES
#  endif
# endif

#endif

// Only compile for Windows targets.
#ifdef CRASH_WIN_ENABLED

# include "windows-crawl.h"

# ifndef CRASH_WIN_NEED_DBGHELP_TYPES
#  ifdef UNICODE
#   define DBGHELP_TRANSLATE_TCHAR
#  endif
#  include <dbghelp.h>
# endif
# include <fcntl.h>
# include <io.h>
# ifdef TARGET_COMPILER_CYGWIN
# include <sys/cygwin.h>
# endif

/**
 * @brief
 *  The maximum error message buffer size for printing Windows error strings.
 */
# define CRASH_WIN_BUFFER_DEFAULT_TCHAR_BUFFER_SIZE (256)

/**
 * @brief Default debugger command to be executed.
 *
 * Gcc-compatible compilers will run gdb, while msvc compilers will run cdb.
 *
 * @see debugger_t::m_start_command
 */
# ifndef TARGET_COMPILER_VC
#  define CRASH_WIN_DEBUGGER_DEFAULT_START_COMMAND \
    "\"gdb.exe\"" \
    " -ex \"set confirm off\"" \
    " -ex \"attach %lu\"" \
    " -ex \"c\""
# else
#  define CRASH_WIN_DEBUGGER_DEFAULT_START_COMMAND \
    "\"cdb.exe\"" \
    " -y \".\" -lines -pd -g -p %lu"
# endif

/**
 * @brief Default "init" commands to be sent to the debugger.
 *
 * @see debugger_t::m_interactive_commands_init
 */
# ifndef TARGET_COMPILER_VC
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_INIT \
    {"c", 0}
# else
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_INIT \
    {"g", 0}
# endif

/**
 * @brief Default "print" commands to be sent to the debugger.
 *
 * @see debugger_t::m_interactive_commands_print
 */
# ifndef TARGET_COMPILER_VC
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_PRINT \
    {"thread apply all bt full", "c", 0}
# else
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_PRINT \
    {"~*e !for_each_frame dv /t /n", "g", 0}
# endif

/**
 * @brief Default "quit" commands to be sent to the debugger.
 *
 * @see debugger_t::m_interactive_commands_print
 */
# ifndef TARGET_COMPILER_VC
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_QUIT \
    {"detach", "q", 0}
# else
#  define CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_QUIT \
    {"qd", 0}
# endif

/*
 * Dynamic library (Windows DLL) related defines:
 *  * The names of known dbghelp-compatible DLLs to use.
 *  * Function names to use when grabbing pointers from the DLLs
 */
/** @brief The name of the standard Windows dbghelp.dll. */
# define CRASH_WIN_DLL_DBGHELP_DLL_NAME "dbghelp.dll"
/** @brief  SymCleanup function name */
# define CRASH_WIN_DLL_DBGHELP_SYMCLEANUP_NAME "SymCleanup"
/** @brief  SymFromAddr function name */
# define CRASH_WIN_DLL_DBGHELP_SYMFROMADDR_NAME "SymFromAddr"
/** @brief  SymGetLineFromAddr64 function name */
# define CRASH_WIN_DLL_DBGHELP_SYMGETLINEFROMADDR64_NAME "SymGetLineFromAddr64"
/** @brief  SymInitialize function name */
# define CRASH_WIN_DLL_DBGHELP_SYMINITIALIZE_NAME "SymInitialize"
/** @brief  SymSetOptions function name */
# define CRASH_WIN_DLL_DBGHELP_SYMSETOPTIONS_NAME "SymSetOptions"
/** @brief  UnDecorateSymbolName function name */
# define CRASH_WIN_DLL_DBGHELP_UNDECORATESYMBOLNAME_NAME "UnDecorateSymbolName"
/** @brief  CaptureStackBackTrace function name */
# define CRASH_WIN_DLL_KERNEL32_CAPTURESTACKBACKTRACE_NAME \
    "RtlCaptureStackBackTrace"
/** @brief The name of the kernel32 dll. */
# define CRASH_WIN_DLL_KERNEL32_DLL_NAME "kernel32.dll"
/** @brief  GetProcessId function name */
# define CRASH_WIN_DLL_KERNEL32_GETPROCESSID_NAME "GetProcessId"
/**
 * @brief The name of Drmingw's mgwhelp.dll (preferred, LGPLv2)
 *
 * @see <a href=https://github.com/jrfonseca/drmingw>Dr. Mingw Homepage</a>
 */
# define CRASH_WIN_DLL_MGWHELP_DLL_NAME "mgwhelp.dll"

/**
 * @brief
 *  Defined if the target toolchain has access to Windows invalid parameter
 *  handler.
 *
 * Note that this checks for the existence of a macro which was introduced in
 * the same MSVCRT version in which _set_invalid_parameter_handler was
 * introduced (0x800, VS 2005).
 *
 * If the target toolchain defines this macro without also ensuring that
 * a declaration and definition for _set_invalid_parameter_handler exists,
 * there may be problems compiling and/or linking.  Notably, this is an issue
 * for older mingw-w64 distributions when linking against the default MSVCRT.
 */
# if defined(_WRITE_ABORT_MSG) || defined(DOXYGEN_ONLY)
#  define CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
# endif

# ifdef DOXYGEN_ONLY
/**
 * @brief Define this macro to print extra stack trace error information.
 *
 * When a symbol or line cannot be obtained during a stack trace, a error code
 * and corresponding error message will be printed instead of simply the default
 * "N/A".
 */
#  define CRASH_WIN_STACK_TRACE_PRINT_SYMBOL_ERRORS
# endif

/**
 * @brief The maximum frames we can capture.
 *
 * WinXP/S2k3 have a lower limit than newer versions of Windows, so just use
 * that lower limit as our max.
 *
 * @todo: Determine the max stack frames based on win version.
 * @todo:  Sync crawl's max stack frames with Win max (i.e., use the lower one)
 */
# define CRASH_WIN_STACK_TRACE_MAX_STACK_FRAMES (63)

/**
 * @brief
 *  The size of the symbol name buffer for Windows SYMBOL_INFO structures.
 */
# define CRASH_WIN_SYMBOL_NAME_BUFFER_SIZE (256)

// Some compilers don't include headers for dbghelp.h.  We need some basics.
# ifdef CRASH_WIN_NEED_DBGHELP_TYPES

// Define the expected types based on the DbgHelp API specification in MSDN
// http://msdn.microsoft.com/en-us/library/windows/desktop/ms679292.aspx

// Flags
/** @brief  Symbol option - deferred loads */
#define SYMOPT_DEFERRED_LOADS (0x00000004)
/** @brief  Symbol options - load line numbers */
#define SYMOPT_LOAD_LINES (0x00000010)
/** @brief  Undecoration/demangle option -- perform complete undecoration */
#define UNDNAME_COMPLETE (0x0000)

/** @brief The IMAGEHLP_LINE64 used by DbgHelp functions */
typedef struct _IMAGEHLP_LINE64
{
    DWORD SizeOfStruct;
    PVOID Key;
    DWORD LineNumber;
    PTSTR FileName;
    DWORD64 Address;
} IMAGEHLP_LINE64;
/** @brief A pointer to a IMAGEHLP_LINE64 struct. */
typedef IMAGEHLP_LINE64 *PIMAGEHLP_LINE64;

// SYMBOL_INFO type
/** @brief The SYMBOL_INFO used by DbgHelp functions */
typedef struct _SYMBOL_INFO
{
    ULONG SizeOfStruct;
    ULONG TypeIndex;
    ULONG64 Reserved[2];
    ULONG Index;
    ULONG Size;
    ULONG64 ModBase;
    ULONG Flags;
    ULONG64 Value;
    ULONG64 Address;
    ULONG Register;
    ULONG Scope;
    ULONG Tag;
    ULONG NameLen;
    ULONG MaxNameLen;
    TCHAR Name[1];
} SYMBOL_INFO;
/** @brief A pointer to a SYMBOL_INFO struct. */
typedef SYMBOL_INFO *PSYMBOL_INFO;

# endif

/**
 * @brief The internal namespace for the file.
 *
 * Contains the internal implementation details, with the exception of the
 * crash_handler_platform_specific_t class, which was forward-declared in
 * crash.h.
 */
namespace crash_win
{

// Function pointers types for loading from kernel32.dll
/** @brief Function pointer type for CaptureStackBackTrace */
typedef USHORT (WINAPI *func_CaptureStackBackTrace_t)(
        ULONG FramesToSkip, ULONG FramesToCapture, PVOID *BackTrace,
        PULONG BackTraceHash);
/** @brief Function pointer type for GetProcessId */
typedef DWORD (WINAPI *func_GetProcessId_t)(HANDLE Process);
// Function pointers types for loading from the dbghelp.dll API
/** @brief Function pointer type for SymInitialize */
typedef BOOL (WINAPI *func_SymInitialize_t)(HANDLE hProcess,
        PCSTR UserSearchPath, BOOL fInvadeProcess);
/** @brief Function pointer type for SymCleanup */
typedef BOOL (WINAPI *func_SymCleanup_t)(HANDLE hProcess);
/** @brief Function pointer type for SymSetOptions */
typedef BOOL (WINAPI *func_SymSetOptions_t)(DWORD SymOptions);
/** @brief Function pointer type for SymFromAddr */
typedef BOOL (WINAPI *func_SymFromAddr_t)(HANDLE hProcess,
        DWORD64 Address, PDWORD64 Displacement, PSYMBOL_INFO Symbol);
/** @brief Function pointer type for SymGetLineFromAddr64 */
typedef BOOL (WINAPI *func_SymGetLineFromAddr64_t)(HANDLE hProcess,
        DWORD64 dwAddr, PDWORD pdwDisplacement, PIMAGEHLP_LINE64 Line);
/** @brief Function pointer type for UnDecorateSymbolName */
typedef BOOL (WINAPI *func_UnDecorateSymbolName_t)(
        PCSTR DecoratedName, PSTR UnDecoratedName, DWORD UndecoratedLength,
        DWORD Flags);

/**
 * @brief Defines a symbol structure which can be allocated statically.
 *
 * SYMBOL_INFO expects memory available after the structure for a name buffer.
 * Thus, it is important that this struct is POD, as the memory layout is
 * important.
 */
struct symbol_info_t
{
    /** @brief The core DbgHelp symbol. */
    SYMBOL_INFO symbol;
    /** @brief The name buffer (note: one TCHAR is already in symbol) */
    TCHAR name_buffer[CRASH_WIN_SYMBOL_NAME_BUFFER_SIZE - 1];

    /**
     * @brief Helper function for initializing the struct.
     *
     * Zeros out the structure members and initializes the structure as
     * necessary for use with DbgHelp functions.  May be used to both initialize
     * and wipe.
     */
    void init();
};

/**
 * @brief Provide a mechanism for attaching a debugger to the current process.
 *
 * Contains the necessary data and methods for attaching a debugger to the
 * current process in order to obtain debugging information just-in-time.
 */
class debugger_t
{
public:
    /**
     * @brief Basic constructor which performs minimal initialization.
     *
     * Note that when using this constructor, the debugger must be further
     * initialized must further initialized via @p init() and @p set_buffers()
     * for correct operation.
     */
    debugger_t(const char *start_command,
            const char *interactive_commands_init[],
            const char *interactive_commands_print[],
            const char *interactive_commands_quit[]);

    /**
     * @brief Constructor which performs advanced initialization.
     *
     * Sets the object's buffers to the passed buffers and calls init().  When
     * using this constructor, @p call() may be immediately invoked afterwards.
     */
    debugger_t(const char *start_command,
            const char *interactive_commands_init[],
            const char *interactive_commands_print[],
            const char *interactive_commands_quit[],
            TCHAR *error_message_buffer, size_t error_message_buffer_size,
            TCHAR *command_buffer, size_t command_buffer_size);

    /**
     * @brief Destroys the debugger.
     *
     * Note: Does not deallocate any of the buffers passed in via @p
     * set_buffers or the corresponding constructor.
     */
    ~debugger_t();

    /**
     * @brief
     *  Launch a debugger process which outputs to @p debugger_output_file.
     *
     * The new process will attempt to execute @p debugger_start_command and,
     * if successfully launched, be sent the passed @p
     * debugger_interactive_command parameter contents via stdin.
     *
     * Windows XP or later is required, as the command is expected to return
     * from execution.  Earlier Windows versions cannot detach a debugger
     * without killing the debugged process.
     *
     * Note: A newline will be automatically appended to each interactive
     * command.
     *
     * @par Warning
     *  Failure to issue a quit command to debugger either via command line
     *  arguments present in @p debugger_start_command or via interactive
     *  commands will cause this function block indefinitely waiting for the
     *  debugger to exit!
     *
     * @param debugger_output_file
     *  The file that the debugger's stdout/stderr will be written to.
     * @param progress_file
     *  The stream to write progress information to.
     *
     * @return Zero on success, Windows Error Code on failure.
     */
    DWORD call(FILE *debugger_output_file, FILE *progress_file);

    /**
     * @brief Performs advanced initialization on the object.
     */
    DWORD init();

    /**
     * @brief Sets the buffers to be used by the object.
     *
     * Note that the passed buffers may point to the same address if desired.
     *
     * @param error_message_buffer
     *  A pre-allocated buffer for writing error messages.
     * @param error_message_buffer_size
     *  The size of @p error_message_buffer.
     * @param command_buffer
     *  A pre-allocated buffer used to invoke the actual debugger process.
     * @param command_buffer_size
     *  The size of @p command_buffer.
     */
    void set_buffers(TCHAR *error_message_buffer,
            size_t error_message_buffer_size, TCHAR *command_buffer,
            size_t command_buffer_size);

private:
    /**
     * @param Helper function to write an array of commands to a stream.
     *
     * Writes out the passed commands to @p file. A newline will be
     * automatically appended to each command in the array.
     *
     * @param file
     *  The stream to write the commands to.
     * @param echo
     *  The stream to echo the commands to.
     *  If a null pointer is passed, commands will not be echoed.
     * @param commands
     *  A null-pointer-terminated array of command strings.
     */
    static void _write_commands(FILE *file, FILE *echo, const char *commands[]);

    /**
     * @brief
     *  A pointer to the buffer to use for the debugger command invocation.
     */
    TCHAR *m_command_buffer;

    /** @brief The size of the command buffer, in TCHARS. */
    size_t m_command_buffer_size;

    /**
     * @brief
     *  A pointer to a buffer to use when writing windows error messages.
     */
    TCHAR *m_error_message_buffer;

    /** @brief The size of the buffer, in TCHARs. */
    size_t m_error_message_buffer_size;

    /** @brief GetProcessId function pointer */
    func_GetProcessId_t m_func_GetProcessId;

    /** @brief Zero if this struct is initialized, nonzero otherwise */
    DWORD m_init_error;

    /**
     * @brief Interactive debugger commands for initialization.
     *
     * A null-pointer-terminated array of command strings to send to the
     * debugger's stdin.
     */
    const char **m_interactive_commands_init;

    /**
     * @brief Interactive debugger commands for printing debugging data.
     *
     * A null-pointer-terminated array of command strings to send to the
     * debugger's stdin.
     */
    const char **m_interactive_commands_print;

    /**
     * @brief Interactive debugger commands for detaching and quitting.
     *
     * A null-pointer-terminated array of command strings to send to the
     * debugger's stdin.
     */
    const char **m_interactive_commands_quit;

    /**
     * @brief The debugger command used to start the debugger process.
     *
     * This command is expected to have one printf token, %ul, to which the
     * current process ID will be written (for debugger attachment).
     */
    const char *m_start_command;
};

/**
 * @brief Contains data necessary for performing a stack trace on Windows.
 *
 * Contains necessary function pointers and buffers for performing a Windows
 * stack trace using a DbgHelp.dll-compatible dll.
 */
class stack_trace_t
{
public:
    /**
     * Close and clean up any system resources opened by init_dbghelp.
     */
    void close_dbghelp();

    /**
     * Takes care of loading DLLs and initializing the dbghelp library.
     *
     * @return Zero on init success, nonzero on error.
     */
    DWORD init_dbghelp();

    /**
     * Assigns the passed buffers as the buffers to be used by the object.
     *
     * @param error_message_buffer
     *  A pre-allocated buffer for writing error messages.
     * @param error_message_buffer_size
     *  The size of @p error_message_buffer.
     * @param undecorated_name_buffer
     *  A pre-allocated buffer used to undecorate (demangle) symbol names.
     * @param undecorated_name_buffer_size
     *  The size of @p undecorated_name_buffer.
     * @param current_stack_frames_buffer
     *  A pre-allocated buffer for storing stack frame addresses.
     * @param current_stack_frames_buffer_size
     *  The size of @p current_stack_frames_buffer_size.
     */
    void set_buffers(TCHAR *error_message_buffer,
            size_t error_message_buffer_size, TCHAR *undecorated_name_buffer,
            size_t undecorated_name_buffer_size,
            PVOID *current_stack_frames_buffer,
            size_t current_stack_frames_buffer_size);

    /**
     * @brief Create a stack_trace_t object with minimal initialization.
     *
     * The dbghelp subsystem is not initialized with this constructor, nor
     * are any buffers allocated.
     */
    stack_trace_t();

    /**
     * @brief Create a stack_trace_t object with advanced initialization.
     *
     * The dbghelp subsystem is automatically initialized by this constructor.
     *
     * @see set_buffers
     */
    stack_trace_t(TCHAR *error_message_buffer, size_t error_message_buffer_size,
            TCHAR *undecorated_name_buffer, size_t undecorated_name_buffer_size,
            PVOID *current_stack_frames_buffer,
            size_t current_stack_frames_buffer_size);

    /**
     * @brief Destroys the stack_trace_t, first performing necessary cleanup.
     *
     * Any system resources opened or initialized by _stack_trace_init will be
     * destroyed and freed.
     */
    ~stack_trace_t();

    /**
     * @brief Perform a stack trace, writing the results to the passed stream.
     *
     * @param output_file
     *  The stream to which the stack trace results will be written.
     * @param progress_file
     *  The stream to which progress information will be written.
     * @param frames_to_ignore
     *  The number of frames on the top of the stack to ignore.
     */
    void write_to_file(FILE *output_file, FILE *progress_file,
            int frames_to_ignore);

private:
    /** @brief A buffer for the current captured stack frame pointers. */
    PVOID *m_current_stack_frames_buffer;

    /** @brief The size of the current stack frames buffer, in PVOIDs */
    size_t m_current_stack_frames_buffer_size;

    /** @brief Current line information. */
    IMAGEHLP_LINE64 m_current_line_info;

    /** @brief Current symbol information. */
    symbol_info_t m_current_symbol;

    /** @brief The name of the DbgHelp compatible-dll currently in use. */
    const char *m_dbghelp_dll_name;

    /** @brief A handle to the DbgHelp DLL (or API-compatible DLL). */
    HINSTANCE m_dbghelp_handle;

    /** A pointer to a buffer to use when writing windows error messages */
    TCHAR *m_error_message_buffer;

    /** The size of the buffer */
    size_t m_error_message_buffer_size;

    // Pointers for needed functions.
    /** @brief CaptureStackBackTrace function pointer */
    func_CaptureStackBackTrace_t m_func_CaptureStackBackTrace;

    /** @brief SymInitialize function pointer */
    func_SymInitialize_t m_func_SymInitialize;

    /** @brief SymCleanup function pointer */
    func_SymCleanup_t m_func_SymCleanup;

    /** @brief SymFromAddr function pointer */
    func_SymFromAddr_t m_func_SymFromAddr;

    /** @brief SymGetLineFromAddr64 function pointer */
    func_SymGetLineFromAddr64_t m_func_SymGetLineFromAddr64;

    /** @brief SymSetOptions function pointer */
    func_SymSetOptions_t m_func_SymSetOptions;

    /** @brief UnDecorateSymbolName function pointer */
    func_UnDecorateSymbolName_t m_func_UnDecorateSymbolName;

    /** Zero if this struct is initialized, nonzero otherwise */
    DWORD m_init_error;

    /** Zero if SymInitialize called successfully, nonzero otherwise. */
    DWORD m_SymInitialize_error;

    /** @brief A buffer for the undecorated (demangled) symbol name. */
    TCHAR *m_undecorated_name_buffer;

    /** @brief The size of the command buffer */
    size_t m_undecorated_name_buffer_size;
};

/**
 * @brief Used for printing out Windows error messages.
 *
 * A plain old data structure used for storing error codes and printing the
 * corresponding error messages.  Defines some methods for consistent printing
 * of these messages.
 */
struct windows_error_t
{
    /** @brief The current windows error code. */
    DWORD code;

    /** @brief The buffer to which error messages will be written. */
    TCHAR *message_buffer;

    /** @brief The size of the message buffer, in TCHARs */
    size_t message_buffer_size;

    /**
     * @brief Get a error message for the current Windows error code.
     *
     * Retrieve the Windows system error message corresponding to the error
     * code in @p error, placing the results in @p message_buffer.  If there
     * is an error while retrieving an error string, the message buffer will be
     * safe to print.
     *
     * @return
     *  The size of the string contained in @p message_buffer after the call
     *  (not including the null character).
     */
     DWORD get_message();

    /**
     * @brief Get and print a basic formatted Windows error message to @p file.
     *
     * Prints the error message to the passed file using GetLastError and
     * FormatMessage in a consistent manner.  The error code and message will
     * be stored in @p error and @p message_buffer, respectively.
     *
     * @param file
     *  The file to write the error message to.
     * @param preamble
     *  A brief context of the error.
     *
     * @return The error code returned by GetLastError.
     */
    DWORD get_and_print_basic(FILE *file, const char *preamble);

    /**
     * @brief Print a pre-formatted Windows message to @p file.
     *
     * Simply prints whatever error message is currently stored.  This is
     * useful for when a single error message needs to be printed to multiple
     * streams.
     *
     * @param file
     *  The file to write the error message to.
     * @param preamble
     *  A brief context of the error.
     */
    void print_basic(FILE *file, const char *preamble);

    /**
     * @brief
     *  Print a generic error message in the same format as @p print_basic.
     *
     * @param file
     *  The file to write the error message to.
     * @param preamble
     *  A brief context of the error.
     * @param code
     *  The error code.
     * @param message
     *  A human-readable error message for @p code.
     */
    static void print_generic_basic(FILE *file, const char *preamble
            , int code, const char *message);
};

// see declaration
void debugger_t::_write_commands(FILE *file, FILE *echo,
        const char *commands[])
{
    int i = 0;
    const char* cur_command;

    // Print commands until a null pointer is reached
    for (cur_command = commands[0]; cur_command; cur_command = commands[++i])
    {
        // Write and flush
        fprintf(file, "%s\n", cur_command);
        fflush(file);
        // Echo if a non-null stream was provided
        if (echo)
        {
            fprintf(echo, "%s\n", cur_command);
            fflush(echo);
        }
    }
}

// see declaration
DWORD debugger_t::call(FILE *debugger_output_file, FILE *progress_file)
{
    // Variable to use when checking various return codes
    windows_error_t error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };

    // The caller's PID
    DWORD current_pid;

    // Windows handle of the passed file stream
    int debugger_output_file_as_fd;
    HANDLE debugger_output_file_as_handle;

    // Windows process/handle information
    // Windows process information (used with CreateProcess)
    PROCESS_INFORMATION debugger_process_info;
    // Pipe security attributes
    SECURITY_ATTRIBUTES debugger_sec_attr;
    // Windows process startup information (used with CreateProcess)
    STARTUPINFO debugger_startup_info;
    // The debugger input handle which crawl will write to.
    HANDLE debugger_stdin_write_handle = NULL;
#ifdef TARGET_COMPILER_CYGWIN
    // The special Cygwin device name for the write side of a pipe
    char debugger_stdin_write_cygname[] = "/dev/pipew";
#endif
    // A intermediate file descriptor for debugger_stdin_write_file.
    int debugger_stdin_write_fd = -1;
    // A stream for conveniently writing to gdb_input_write_handle.
    FILE *debugger_stdin_write_file = 0;
    // The debugger input handle which will become the debugger's stdin
    HANDLE debugger_stdin_read_handle = NULL;

    // The return code of the debugger process
    DWORD debugger_return_code;

    // See if launching the debugger should be attempted.
    //  * Need to be able to find out the current process ID in order to attach
    //    (note: Detaching a debugger without exiting requires the same minimum
    //           OS version as GetProcessId.)
    if (!m_init_error)
    {
        // Grab our current PID (to be sent to gdb)
        current_pid = m_func_GetProcessId(GetCurrentProcess());

        // Grab a Windows handle for our passed file argument
#ifndef TARGET_COMPILER_CYGWIN
        debugger_output_file_as_fd = _fileno(debugger_output_file);
#else
        debugger_output_file_as_fd = fileno(debugger_output_file);
#endif
        debugger_output_file_as_handle = (HANDLE) _get_osfhandle(
                debugger_output_file_as_fd);

        // Set the attributes for the pipes (inherited by child, default)
        debugger_sec_attr.nLength = sizeof(SECURITY_ATTRIBUTES);
        debugger_sec_attr.bInheritHandle = TRUE;
        debugger_sec_attr.lpSecurityDescriptor = NULL;

        // Create the pipe for writing to the debugger's stdin
        error.code = !CreatePipe(&debugger_stdin_read_handle,
                &debugger_stdin_write_handle, &debugger_sec_attr, 0);
        if (!error.code)
        {
            // Create a stream for convenience.
            // First create the intermediate file descriptor, then the FILE *
#ifndef TARGET_COMPILER_CYGWIN
            debugger_stdin_write_fd = _open_osfhandle(
                    (intptr_t) debugger_stdin_write_handle, O_RDONLY);
#else
            debugger_stdin_write_fd = cygwin_attach_handle_to_fd(
                    debugger_stdin_write_cygname, -1,
                    debugger_stdin_write_handle, 0, GENERIC_WRITE);
#endif
            debugger_stdin_write_file = fdopen(debugger_stdin_write_fd, "w");
            if (!debugger_stdin_write_file)
                error.code = 1;
        }

        if (!error.code)
        {
            // Initialize the process structures
            ZeroMemory(&debugger_process_info, sizeof(PROCESS_INFORMATION));
            ZeroMemory(&debugger_startup_info, sizeof(STARTUPINFO));
            debugger_startup_info.cb = sizeof(STARTUPINFO);
            debugger_startup_info.dwFlags |= STARTF_USESTDHANDLES;

            // Set up the child process's stdin/stdout/stderr
            debugger_startup_info.hStdInput = debugger_stdin_read_handle;
            debugger_startup_info.hStdOutput = debugger_output_file_as_handle;
            debugger_startup_info.hStdError = debugger_output_file_as_handle;

            // Create the command string using the command buffer
            // (Attach debugger to the current process)
#ifndef TARGET_COMPILER_CYGWIN
            // MSVC _snprintf (note: not C99-compliant!)
            _snprintf(m_command_buffer, m_command_buffer_size, m_start_command,
                    current_pid);
            // Guarantee null termination.
            m_command_buffer[m_command_buffer_size - 1] = '\0';
#else
            // Cygwin has a C99-style snprintf
            snprintf(m_command_buffer, m_command_buffer_size, m_start_command,
                    current_pid);
#endif

            // Indicate which command is being executed.
            fprintf(progress_file, "Executing command:\n  %s\n",
                    m_command_buffer);
            fflush(progress_file);
            fprintf(debugger_output_file, "Executing command:\n  %s\n",
                    m_command_buffer);

            // Create the process for the debugger!
            error.code = !CreateProcess(NULL, // No AppName
                    m_command_buffer,
                    NULL,                     // Default ProcessAttributes
                    NULL,                     // Default ThreadAttributes
                    TRUE,                     // Inherit handles
                    CREATE_NO_WINDOW,         // Don't spam out extra windows.
                    NULL,                     // Default environment
                    NULL,                     // Default current directory
                    &debugger_startup_info,
                    &debugger_process_info);
        }

        // Check if our process has been created.
        if (!error.code)
        {
            // Give some feedback about the debugger's state.
            fprintf(progress_file, "Debugger is running...\n");
            fflush(progress_file);

            //
            // Wait for the debugger to attach and continue.
            //
            // The cdb debugger has syntactical problems with sending nontrivial
            // commands via command line arguments.  So, send the nontrivial
            // commands in an "interactive" manner.
            //
            // The gdb debugger has problems traversing the stack when the
            // inferior (this program) is stuck in a Windows
            // WaitForSingleObject() call.  This loop and following DebugBreak()
            // calls make sure that gdb performs a backtrace only when the
            // inferior is in a function in which gdb *can* traverse the stack
            // correctly.
            //
            // It is expected that the debugger handles both the start commands
            // and following interactive commands in a synchronous manner.  In
            // particular, if the continue in "attach and continue" executes
            // before the attach completes, the debugging session will deadlock.
            //
            // This may be an issue with GDB versions before 7.4.
            // https://sourceware.org/ml/gdb-patches/2011-09/msg00037.html
            //
            error.code = WAIT_TIMEOUT;
            while(!IsDebuggerPresent() && error.code == WAIT_TIMEOUT)
            {
                // Wait for 100ms, but break if the debugger exits/crashes.
                error.code = WaitForSingleObject(debugger_process_info.hProcess,
                        100);
            }

            // Send each command group in a specific order:
            //    Init -> Print -> Quit
            // Making sure that the debugger is still active along the way

            // Write the "init" commands then break to the debugger.
            if (IsDebuggerPresent())
            {
                FlushFileBuffers(debugger_output_file_as_handle);
                _write_commands(debugger_stdin_write_file, debugger_output_file,
                        m_interactive_commands_init);
                DebugBreak();
            }

            // Write the "print" commands then break to the debugger.
            if (IsDebuggerPresent())
            {
                FlushFileBuffers(debugger_output_file_as_handle);
                _write_commands(debugger_stdin_write_file, debugger_output_file,
                        m_interactive_commands_print);
                DebugBreak();
            }

            // Write the "quit" commands then break to the debugger.
            if (IsDebuggerPresent())
            {
                FlushFileBuffers(debugger_output_file_as_handle);
                _write_commands(debugger_stdin_write_file, debugger_output_file,
                        m_interactive_commands_quit);
                DebugBreak();
            }

            // All done, wait for the debugger to exit.
            // TODO: Enforce a time out?  Also, check the wait result.
            WaitForSingleObject(debugger_process_info.hProcess, INFINITE);
            error.code = !GetExitCodeProcess(debugger_process_info.hProcess,
                    &debugger_return_code);
            if (error.code)
            {
                // Error while getting the exit code...
                error.get_and_print_basic(debugger_output_file,
                        "Error while waiting for debugger to finish");
                error.print_basic(stderr,
                        "Error while waiting for debugger to finish");
            }

            // Clean up process handles.
            if (!CloseHandle(debugger_process_info.hProcess))
            {
                error.get_and_print_basic(stderr,
                        "Error encountered while closing debugger"
                        " process handle");
            }
            if (!CloseHandle(debugger_process_info.hThread))
            {
                error.get_and_print_basic(stderr,
                        "Error encountered while closing debugger"
                        " thread handle");
            }

            fprintf(progress_file, "Debugger finished.\n");
            fflush(progress_file);
        }
        else
        {
            // Failure creating process!
            error.get_and_print_basic(debugger_output_file,
                    "Error executing debugger");
        }

        // Clean up pipe handles / files.
        if (debugger_stdin_write_handle)
        {
            // Clean up process handles.
            if (debugger_stdin_write_file)
            {
                if (fclose(debugger_stdin_write_file))
                {
                    // This isn't a windows error, but print in the same format
                    windows_error_t::print_generic_basic(stderr,
                            "Error closing pipe", errno, strerror(errno));
                }
            }
            else
            {
                if (!CloseHandle(debugger_stdin_write_handle))
                {
                    error.get_and_print_basic(stderr, "Error closing pipe");
                }
            }
        }
        if (debugger_stdin_read_handle)
        {
            // Clean up process handles.
            if (!CloseHandle(debugger_stdin_read_handle))
            {
                error.get_and_print_basic(stderr, "Error closing pipe");
            }
        }

    }
    else
    {
        // OS version not high enough, not attempting to launch debugger.
        error.code = ERROR_INVALID_HANDLE;
        fprintf(debugger_output_file,
                "Not launching debugger:\n  "
                CRASH_WIN_DLL_KERNEL32_GETPROCESSID_NAME
                " is not available on the current system.\n"
                "  (Windows XP or later required)\n");
    }

    return error.code;
}

// see declaration
debugger_t::debugger_t(const char *start_command,
        const char *interactive_commands_init[],
        const char *interactive_commands_print[],
        const char *interactive_commands_quit[])
{
    m_start_command = start_command;
    m_interactive_commands_init = interactive_commands_init;
    m_interactive_commands_print = interactive_commands_print;
    m_interactive_commands_quit = interactive_commands_quit;
    m_func_GetProcessId = 0;
    m_command_buffer = 0;
    m_command_buffer_size = 0;
    m_error_message_buffer = 0;
    m_error_message_buffer_size = 0;
    m_init_error = 1;
}

// see declaration
debugger_t::debugger_t(const char *start_command,
        const char *interactive_commands_init[],
        const char *interactive_commands_print[],
        const char *interactive_commands_quit[], TCHAR *error_message_buffer,
        size_t error_message_buffer_size, TCHAR *command_buffer,
        size_t command_buffer_size)
{
    // Basic init
    m_start_command = start_command;
    m_interactive_commands_init = interactive_commands_init;
    m_interactive_commands_print = interactive_commands_print;
    m_interactive_commands_quit = interactive_commands_quit;
    m_func_GetProcessId = 0;
    m_command_buffer = 0;
    m_command_buffer_size = 0;
    m_error_message_buffer = 0;
    m_error_message_buffer_size = 0;
    m_init_error = 1;

    // Use set_buffers to assign the buffers.
    set_buffers(error_message_buffer, error_message_buffer_size, command_buffer
            , command_buffer_size);

    // Fully initialize.
    init();
}

// see declaration
debugger_t::~debugger_t()
{
    // Nothing extra required.
}

// see declaration
DWORD debugger_t::init()
{
    // Handle to kernel32.dll
    HMODULE kernel32_handle;
    // The current windows error code
    windows_error_t error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };

    // Grab a handle to kernel32 (loaded by each windows process).
    kernel32_handle = GetModuleHandle(CRASH_WIN_DLL_KERNEL32_DLL_NAME);
    if (kernel32_handle)
    {
        // Look up the address for GetProcessId
        m_func_GetProcessId =
                (func_GetProcessId_t) GetProcAddress(kernel32_handle,
                        CRASH_WIN_DLL_KERNEL32_GETPROCESSID_NAME);
        if (m_func_GetProcessId)
        {
            // All good, assign the buffers
            m_init_error = 0;
        }
        else
        {
            error.get_and_print_basic(stderr,
                    CRASH_WIN_DLL_KERNEL32_GETPROCESSID_NAME " unavailable.");
        }
    }
    else
    {
        error.get_and_print_basic(stderr, "Could not get handle for "
                CRASH_WIN_DLL_KERNEL32_DLL_NAME);
    }

    // If any errors were encountered, set the field in the struct.
    if (error.code)
        m_init_error = error.code;

    return error.code;
}

// see declaration
void debugger_t::set_buffers(TCHAR *error_message_buffer,
        size_t error_message_buffer_size, TCHAR *command_buffer,
        size_t command_buffer_size)
{
    m_error_message_buffer = error_message_buffer;
    m_error_message_buffer_size = error_message_buffer_size;
    m_command_buffer = command_buffer;
    m_command_buffer_size = command_buffer_size;
}

// see declaration
void stack_trace_t::close_dbghelp()
{
    // Handle to the current process
    HANDLE cur_process;
    // Current windows error code
    windows_error_t error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };

    // Clean up symbols
    if (!m_SymInitialize_error)
    {
        cur_process = GetCurrentProcess();
        error.code = !m_func_SymCleanup(cur_process);
        if (error.code)
        {
            // Print the error to stderr.
            error.get_and_print_basic(stderr,
                    "Error cleaning up Windows symbol handler");
        }
        // Mark as not initialized
        m_SymInitialize_error = 1;
    }
    // Close the dbghelp library
    if (m_dbghelp_handle)
    {
        // Close the DLL
        error.code = !FreeLibrary(m_dbghelp_handle);
        if (error.code)
        {
            // Print the error to stderr.
            fprintf(stderr, "Error encountered while closing ");
            error.get_and_print_basic(stderr, m_dbghelp_dll_name);
        }
        m_dbghelp_handle = 0;
        m_dbghelp_dll_name = 0;
    }
}

// see declaration
DWORD stack_trace_t::init_dbghelp()
{
    // Handle to the current process
    HANDLE cur_process;
    // Handle to kernel32.dll
    HMODULE kernel32_handle;
    // The current windows error code
    windows_error_t error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };
    // The error code encountered while loading mgwhelp.dll
    windows_error_t mgwhelp_error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };

    // Only attempt initialization if not already initialized.
    if (m_init_error)
    {
        // Grab a handle to kernel32 (loaded by each windows process).
        kernel32_handle = GetModuleHandle(CRASH_WIN_DLL_KERNEL32_DLL_NAME);
        if (kernel32_handle)
        {
            // Look up CaptureStackBackTrace.
            m_func_CaptureStackBackTrace =
                    (func_CaptureStackBackTrace_t) GetProcAddress(
                            kernel32_handle,
                            CRASH_WIN_DLL_KERNEL32_CAPTURESTACKBACKTRACE_NAME);
            if (!m_func_CaptureStackBackTrace)
            {
                error.get_and_print_basic(stderr,
                        CRASH_WIN_DLL_KERNEL32_CAPTURESTACKBACKTRACE_NAME
                        " unavailable");
            }
        }
        else
        {
            error.get_and_print_basic(stderr, "Could not get handle for "
                    CRASH_WIN_DLL_KERNEL32_DLL_NAME);
        }

        // If CaptureStackBackTrace was found, load in dbghelp
        if (!error.code)
        {
            // CaptureStackBackTrace is available, continue on!
            // Try to load the DLL, preferring mgwhelp.dll if possible
            m_dbghelp_dll_name = CRASH_WIN_DLL_MGWHELP_DLL_NAME;
            m_dbghelp_handle = LoadLibrary(CRASH_WIN_DLL_MGWHELP_DLL_NAME);

            if (!m_dbghelp_handle)
            {
                // Try falling back to the standard dbghelp.dll.
                mgwhelp_error.code = GetLastError();
                m_dbghelp_dll_name = CRASH_WIN_DLL_DBGHELP_DLL_NAME;
                m_dbghelp_handle = LoadLibrary(CRASH_WIN_DLL_DBGHELP_DLL_NAME);
            }

            if (m_dbghelp_handle)
            {
                // DLL loaded successfully, continue initialization.

                // Get the function pointers we need
                m_func_SymInitialize =
                        (func_SymInitialize_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_SYMINITIALIZE_NAME);
                m_func_SymCleanup =
                        (func_SymCleanup_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_SYMCLEANUP_NAME);
                m_func_SymSetOptions =
                        (func_SymSetOptions_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_SYMSETOPTIONS_NAME);
                m_func_SymFromAddr =
                        (func_SymFromAddr_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_SYMFROMADDR_NAME);
                m_func_SymGetLineFromAddr64 =
                        (func_SymGetLineFromAddr64_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_SYMGETLINEFROMADDR64_NAME);
                m_func_UnDecorateSymbolName =
                        (func_UnDecorateSymbolName_t) GetProcAddress(
                                m_dbghelp_handle,
                                CRASH_WIN_DLL_DBGHELP_UNDECORATESYMBOLNAME_NAME);

                // Verify that all of these function pointers are valid
                if (m_func_SymInitialize && m_func_SymCleanup
                        && m_func_SymSetOptions
                        && m_func_SymFromAddr
                        && m_func_SymGetLineFromAddr64
                        && m_func_UnDecorateSymbolName)
                {
                    // Initialize debugging symbols for the current process.
                    cur_process = GetCurrentProcess();
                    // Our variables
                    m_current_line_info.SizeOfStruct =
                            sizeof(IMAGEHLP_LINE64);
                    m_current_symbol.init();
                    // System variables
                    m_func_SymSetOptions(SYMOPT_DEFERRED_LOADS
                            | SYMOPT_LOAD_LINES);
                    error.code = !m_func_SymInitialize(cur_process, NULL, TRUE);
                    m_SymInitialize_error = error.code;

                    if (error.code)
                    {
                        // Not good -- print the error to stderr.
                        error.get_and_print_basic(stderr,
                                "Error initializing Windows symbol handler");
                        m_SymInitialize_error = error.code;
                    }
                }
                else
                {
                    // Could not find all of the necessary functions in the DLL
                    fprintf(stderr, "Could not find all functions in %s!\n",
                            m_dbghelp_dll_name);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_SYMINITIALIZE_NAME,
                            m_func_SymInitialize);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_SYMCLEANUP_NAME,
                            m_func_SymCleanup);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_SYMSETOPTIONS_NAME,
                            m_func_SymSetOptions);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_SYMFROMADDR_NAME,
                            m_func_SymFromAddr);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_SYMGETLINEFROMADDR64_NAME,
                            m_func_SymGetLineFromAddr64);
                    fprintf(stderr, "  %s : %p\n",
                            CRASH_WIN_DLL_DBGHELP_UNDECORATESYMBOLNAME_NAME,
                            m_func_UnDecorateSymbolName);
                }
            }
            else
            {
                // Note that neither DLL could be loaded.
                // TODO: It might be nice to save these code for the logs.
                error.code = GetLastError();
                fprintf(stderr, "Error loading DbgHelp library.  Tried:\n");
                mgwhelp_error.get_message();
                fprintf(stderr, "  %s; e:%lu: %s\n",
                        CRASH_WIN_DLL_MGWHELP_DLL_NAME, mgwhelp_error.code,
                        mgwhelp_error.message_buffer);
                error.get_message();
                fprintf(stderr, "  %s; e:%lu: %s\n",
                        CRASH_WIN_DLL_DBGHELP_DLL_NAME, error.code,
                        error.message_buffer);
            }
        }
        // Save the result of the initialization
        m_init_error = error.code;
    }

    return error.code;
}

// see declaration
void stack_trace_t::set_buffers(TCHAR *error_message_buffer,
        size_t error_message_buffer_size, TCHAR *undecorated_name_buffer,
        size_t undecorated_name_buffer_size,
        PVOID *current_stack_frames_buffer,
        size_t current_stack_frames_buffer_size)
{
    // Copy over the supplied buffer information
    m_error_message_buffer = error_message_buffer;
    m_error_message_buffer_size = error_message_buffer_size;
    m_undecorated_name_buffer = undecorated_name_buffer;
    m_undecorated_name_buffer_size = undecorated_name_buffer_size;
    m_current_stack_frames_buffer = current_stack_frames_buffer;
    m_current_stack_frames_buffer_size = current_stack_frames_buffer_size;
}

// see declaration
stack_trace_t::stack_trace_t()
{
    // Invalidate the buffers
    m_error_message_buffer = 0;
    m_error_message_buffer_size = 0;
    m_undecorated_name_buffer = 0;
    m_undecorated_name_buffer_size = 0;
    m_current_stack_frames_buffer = 0;
    m_current_stack_frames_buffer_size = 0;

    // Mark initialization as failed (since it hasn't been attempted yet)
    m_init_error = 1;
    m_SymInitialize_error = 1;

    // Initialize other members
    m_func_CaptureStackBackTrace = 0;
    m_func_SymCleanup = 0;
    m_func_SymFromAddr = 0;
    m_func_SymGetLineFromAddr64 = 0;
    m_func_SymInitialize = 0;
    m_func_SymSetOptions = 0;
    m_func_UnDecorateSymbolName = 0;
    m_dbghelp_dll_name = 0;
    m_dbghelp_handle = 0;
}

// see declaration
stack_trace_t::stack_trace_t(TCHAR *error_message_buffer,
        size_t error_message_buffer_size, TCHAR *undecorated_name_buffer,
        size_t undecorated_name_buffer_size, PVOID *current_stack_frames_buffer,
        size_t current_stack_frames_buffer_size)
{
    // Copy over the supplied buffer information
    set_buffers(error_message_buffer, error_message_buffer_size,
            undecorated_name_buffer, undecorated_name_buffer_size,
            current_stack_frames_buffer, current_stack_frames_buffer_size);

    // Mark initialization as failed (since it hasn't been attempted yet)
    m_init_error = 1;
    m_SymInitialize_error = 1;

    // Initialize other members
    m_func_CaptureStackBackTrace = 0;
    m_func_SymCleanup = 0;
    m_func_SymFromAddr = 0;
    m_func_SymGetLineFromAddr64 = 0;
    m_func_SymInitialize = 0;
    m_func_SymSetOptions = 0;
    m_func_UnDecorateSymbolName = 0;
    m_dbghelp_dll_name = 0;
    m_dbghelp_handle = 0;

    // Basic member initialization done, now init the dbghelp subsystem
    init_dbghelp();
}

// see declaration
stack_trace_t::~stack_trace_t()
{
    // Clean up the dbghelp library
    close_dbghelp();
}

// see declaration
void stack_trace_t::write_to_file(FILE *output_file, FILE *progress_file,
        int frames_to_ignore)
{
    // The current stack frame
    PVOID cur_frame;
    // A handle to the current process
    HANDLE cur_process;
    // SymGetLineFromAddr64 needs to write to this variable.  Unused otherwise.
    DWORD cur_displacement;
    // The current Windows error code
    windows_error_t error =
    {
            0,
            m_error_message_buffer,
            m_error_message_buffer_size
    };
    // Index variable
    USHORT i;
    // The current number of stack frames available
    USHORT num_frames;
    // The length of the resulting demangled function string
    DWORD demangled_name_len = 0;

    if (!m_init_error)
    {
        // Note which dll is being used.
        fprintf(progress_file, "Obtaining stack trace using %s.\n",
                m_dbghelp_dll_name);
        fflush(progress_file);
        fprintf(output_file, "Obtaining stack trace using %s.\n",
                m_dbghelp_dll_name);

        cur_process = GetCurrentProcess();

        // Clear any existing stack frame pointer data.
        for (i = 0; i < m_current_stack_frames_buffer_size; i++)
            m_current_stack_frames_buffer[i] = NULL;

        // TODO: Use frames_to_ignore?  Ensure buffer size < Windows max?
        // Capture the stack trace.
        num_frames = m_func_CaptureStackBackTrace(0,
                m_current_stack_frames_buffer_size,
                m_current_stack_frames_buffer, NULL);

        // Print number of stack frames captured.
        fprintf(output_file, "Obtained %hu stack frames.\n", num_frames);

        // Now, print the stack data
        for (i = 0; i < num_frames; i++)
        {
            // Print the current frame number
            fprintf(output_file, "#%-2hu ", i);

            // Grab the next frame (already checked for null pointer)
            cur_frame = m_current_stack_frames_buffer[i];

            // Initialize symbol struct (clear) and grab data.
            m_current_symbol.init();
            error.code = !m_func_SymFromAddr(cur_process,
                    (DWORD64) PtrToUlong(cur_frame), 0,
                    &(m_current_symbol.symbol));
            if (!error.code)
            {
                // Print the address (always print 0x, even for zero values)
                fprintf(output_file,
# ifdef __MINGW64_VERSION_MAJOR
                        // C99-style
                        "0x%08llx ",
# else
                        // MSVC-style
                        "0x%08I64x ",
# endif
                        m_current_symbol.symbol.Address);
                // Demangle the symbol name
                demangled_name_len = m_func_UnDecorateSymbolName(
                        m_current_symbol.symbol.Name, m_undecorated_name_buffer,
                        m_undecorated_name_buffer_size, UNDNAME_COMPLETE);
                if (!demangled_name_len)
                {
                    // Demangle failed, terminate the buffer.
                    m_undecorated_name_buffer[0] = '\0';
                }

                // Print out the symbol name
                fprintf(output_file, "in %s:%s at ",
                        m_current_symbol.symbol.Name,
                        m_undecorated_name_buffer);

                // Get the source line for this address and print!
                error.code = !m_func_SymGetLineFromAddr64(cur_process,
                        (DWORD64) PtrToUlong(cur_frame), &cur_displacement,
                        &(m_current_line_info));
                if (!error.code)
                {
                    fprintf(output_file, "%s:%lu",
                            m_current_line_info.FileName,
                            m_current_line_info.LineNumber);
                }
                else
                {
                    fprintf(output_file, "N/A");
# ifdef CRASH_WIN_STACK_TRACE_PRINT_SYMBOL_ERRORS
                    error.code = GetLastError();
                    error.get_message();
                    fprintf(output_file, " (e:%lu: %s)", error.code,
                            error.message_buffer);
# endif
                }

                // Done with this frame
                fprintf(output_file, "\n");
            }
            else
            {
                // Symbol unavailable
                fprintf(output_file, "N/A");
# ifdef CRASH_WIN_STACK_TRACE_PRINT_SYMBOL_ERRORS
                error.code = GetLastError();
                error.get_message();
                fprintf(output_file, " (e:%lu: %s)", error.code,
                        error.message_buffer);
# endif
                fprintf(output_file, "\n");
            }
        }

        // Report on progress.
        fprintf(progress_file, "Stack trace finished.\n");
        fflush(progress_file);

    }
    else
    {
        fprintf(output_file, "Cannot obtain stack trace:\n"
                "  Not initialized or error during initialization.\n");
    }

}

// see declaration
void symbol_info_t::init()
{
    // Clear out any existing data the symbol.
    memset(this, 0, sizeof(symbol_info_t));

    // Initialize size information.
    symbol.SizeOfStruct = sizeof(SYMBOL_INFO);
    symbol.MaxNameLen = CRASH_WIN_SYMBOL_NAME_BUFFER_SIZE;
}

// see declaration
DWORD windows_error_t::get_message()
{
    // The length of the current error string
    DWORD error_str_len;

    // Grab the system error string (let Windows determine the language)
    error_str_len = FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_MAX_WIDTH_MASK, 0,
            code, 0, message_buffer, message_buffer_size, NULL);
    if (error_str_len)
    {
        // Truncate a trailing space if one exists
        if (message_buffer[error_str_len - 1] == ' ')
            message_buffer[error_str_len - 1] = '\0';
    }
    else
    {
        // Error while getting the message, make sure the buffer is safe
        message_buffer[0] = '\0';
    }

    return error_str_len;
}

// see declaration
DWORD windows_error_t::get_and_print_basic(FILE *file,
        const char *preamble)
{
    // Grab the last error code + corresponding message and print.
    code = GetLastError();
    get_message();
    print_basic(file, preamble);

    return code;
}

// see declaration
void windows_error_t::print_basic(FILE *file,
        const char *preamble)
{
    fprintf(file, "%s:\n  e:%lu: %s\n", preamble, code, message_buffer);
}

// see declaration
void windows_error_t::print_generic_basic(FILE *file, const char *preamble,
        int code, const char *message)
{
    fprintf(file, "%s:\n  e:%d: %s\n", preamble, code, message);
}

} // namespace crash_win

/**
 * Contains the windows-specific crash handler functionality.
 *
 * Note: This relies on crash_handler_t being a singleton to ensure that
 *  the subsystems are not initialized more than once.
 */
class crash_handler_platform_specific_t
{
    /**
     * Allow crash_handler_t access to this class's private members.
     */
    friend class crash_handler_t;

public:
    /**
     * Basic constructor.  Allocates default-sized buffers, uses default
     * debugger commands, and initializes the debugger and stack trace
     * structures.
     *
     * @todo: Throw an exception on initialization failure.
     */
    crash_handler_platform_specific_t();

    /**
     * Default destructor.  Frees any memory allocated for buffers and attempts
     * to destroy the debugger/stack_trace structures cleanly.
     */
    ~crash_handler_platform_specific_t();

    /**
     * @brief Free and set @p *data to null if data is a non-null pointer.
     *
     * Note: This should only be used on memory allocated by a call to malloc
     * or similar function (i.e., not allocated by a C++ new call).
     *
     * @param data
     *  A pointer to the data pointer to conditionally free.
     */
    static void free_and_null_if_valid(void **data);

    /**
     * @brief The default debugger command to use for debugging.
     *
     * @see crash_win::debugger_t::m_start_command
     */
    static const char *default_debugger_start_command;

    /**
     * @brief The default "init" commands to be used for debugging.
     *
     * @see crash_win::debugger_t::m_interactive_commands_init
     */
    static const char *default_debugger_interactive_commands_init[];

    /**
     * @brief The default "print" commands to be used for debugging.
     *
     * @see crash_win::debugger_t::m_interactive_commands_print
     */
    static const char *default_debugger_interactive_commands_print[];

    /**
     * @brief The default "quit" commands to be used for debugging.
     *
     * @see crash_win::debugger_t::m_interactive_commands_quit
     */
    static const char *default_debugger_interactive_commands_quit[];

private:

#ifdef CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
    /**
     * @brief A custom invalid parameter handler.
     *
     * Raises a known, valid crash signal in an attempt to provide a usable
     * crash dump.  Note that all arguments are expected to be NULL unless
     * using a debug version of the Windows CRT.
     *
     * @param expression
     *  The argument expression which caused the invalid parameter
     * @param function
     *  The Windows CRT function where the invalid parameter occurred.
     * @param file
     *  The name of the the corresponding Windows CRT file.
     * @param line
     *  The corresponding line number in @p file.
     * @param pReserved
     *  An argument reserved for Windows.
     */
    static void _invalid_parameter_handler_crash(const wchar_t *expression,
            const wchar_t *function, const wchar_t *file, unsigned int line,
            uintptr_t pReserved);
#endif

    /** @brief A buffer for holding memory addresses (void pointers) */
    PVOID *m_buffer_address;

    /** @brief The size of m_buffer_address, in PVOIDs. */
    size_t m_buffer_address_size;

    /** @brief A generic TCHAR buffer for internal use. */
    TCHAR *m_buffer_generic_tchar;

    /** @brief The size of m_buffer_generic_tchar, in TCHARs. */
    size_t m_buffer_generic_tchar_size;

    /**
     * @brief
     *  The internal structure used to implement crash_handler_t::call_gdb().
     */
    crash_win::debugger_t m_debugger;

#ifdef CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
    /** @brief Stores the original invalid parameter handler. */
    _invalid_parameter_handler m_invalid_parameter_handler_original;
#endif

    /**
     * @brief
     *  The internal structure used to implement
     *  crash_handler_t::write_stack_trace().
     */
    crash_win::stack_trace_t m_stack_trace;
};

/**
 * @internal
 * @par Windows
 *  Allocate and initialize Windows resources necessary for handling crashes.
 *  Attempts to load mgwhelp before falling back to the standard dbghelp
 *  implementation.
 */
crash_handler_t::crash_handler_t()
        : abstract_crash_handler_t()
{
    m_platform_specifics = new crash_handler_platform_specific_t();
}

/**
 * @internal
 * @par Windows
 *  Clean up resources allocated for Windows debugging.
 */
crash_handler_t::~crash_handler_t()
{
    delete m_platform_specifics;
}

/**
 * @internal
 * @par Windows
 * Invoke a debugger using the Windows API. Windows XP or later is required.
 * See _CW_CALL_GDB_RAW_COMMAND for more info on which debugger will be
 * executed.
 */
void crash_handler_t::call_gdb(FILE *file)
{
    // Respect crawl's current no_gdb option
    if (!crawl_state.no_gdb)
    {
        // All clear, call the debugger
        m_platform_specifics->m_debugger.call(file, stdout);
    }
    else
        fprintf(file, "  %s\n", crawl_state.no_gdb);
}

/**
 * @internal
 * @par Windows
 *  Write a stack trace in Windows using the DbgHelp API.
 *  <br><br>
 *  This stack trace is a little more detailed than the posix stack trace, as
 *  gdb tend to be less reliable on Windows than on standard posix systems.
 *  The stack trace is formatted in the gdb backtrace style.
 *  <br>
 *  <pre>#10 0x12345678 in some_func:some_func_demangled() at some_file.cc:42
 *  </pre>
 */
void crash_handler_t::write_stack_trace(FILE* file, int ignore_count)
{
    // Used to adjust the passed frame ignore count
    int adjusted_ignore_count = 0;

    // This function defers the actual stack trace to another function
    //  (i.e., it will be adding a stack frame), so ignore_count will be
    //  incremented if it is nonzero.
    if (ignore_count)
        adjusted_ignore_count = ignore_count + 1;

    // Simply call the internal function on the member variable.
    m_platform_specifics->m_stack_trace.write_to_file(file, stdout,
            adjusted_ignore_count);
}

// see declaration
crash_handler_platform_specific_t::crash_handler_platform_specific_t()
        : m_debugger(default_debugger_start_command,
                default_debugger_interactive_commands_init,
                default_debugger_interactive_commands_print,
                default_debugger_interactive_commands_quit), m_stack_trace()
{
    int error = 0;

#ifdef CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
    // Set the invalid parameter handler and save original
    m_invalid_parameter_handler_original = _set_invalid_parameter_handler(
            _invalid_parameter_handler_crash);
#endif

    // Allocate buffers
    // Use a single, large message buffer
    m_buffer_generic_tchar = (TCHAR *) malloc(
            sizeof(TCHAR) * CRASH_WIN_BUFFER_DEFAULT_TCHAR_BUFFER_SIZE);
    m_buffer_generic_tchar_size = CRASH_WIN_BUFFER_DEFAULT_TCHAR_BUFFER_SIZE;
    // Address buffer for stack frames
    m_buffer_address = (PVOID *) malloc(
            sizeof(PVOID) * CRASH_WIN_STACK_TRACE_MAX_STACK_FRAMES);
    m_buffer_address_size = CRASH_WIN_STACK_TRACE_MAX_STACK_FRAMES;

    // Check for successful allocation
    if (m_buffer_generic_tchar && m_buffer_address)
    {
        // All good -- try to initialize the debugger_t and stack_trace_t
        m_debugger.set_buffers(m_buffer_generic_tchar,
                m_buffer_generic_tchar_size, m_buffer_generic_tchar,
                m_buffer_generic_tchar_size);
        m_stack_trace.set_buffers(m_buffer_generic_tchar,
                m_buffer_generic_tchar_size, m_buffer_generic_tchar,
                m_buffer_generic_tchar_size, m_buffer_address,
                m_buffer_address_size);
        error = m_debugger.init();
        error |= m_stack_trace.init_dbghelp();
        if (error)
        {
            fprintf(stderr, "Warning: Crash subsystem initialization"
                    " failure detected.");
        }
    }
    else
    {
        // Malloc failure, clean up any partial allocation
        free_and_null_if_valid((void **)&m_buffer_generic_tchar);
        free_and_null_if_valid((void **)&m_buffer_address);
        fprintf(stderr, "Error allocating memory for crash structs.\n");
    }

    // TODO: throw exceptions on error, leave printing the message for caller
}

// see declaration
crash_handler_platform_specific_t::~crash_handler_platform_specific_t()
{
    // Free any memory allocated for the buffers
    free_and_null_if_valid((void **)&m_buffer_generic_tchar);
    free_and_null_if_valid((void **)&m_buffer_address);

    // Restore the original invalid parameter handler
#ifdef CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
    _set_invalid_parameter_handler(m_invalid_parameter_handler_original);
#endif
}

// see declaration
const char *crash_handler_platform_specific_t::default_debugger_start_command =
        CRASH_WIN_DEBUGGER_DEFAULT_START_COMMAND;

// see declaration
const char *crash_handler_platform_specific_t
                ::default_debugger_interactive_commands_init[] =
                        CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_INIT;

// see declaration
const char *crash_handler_platform_specific_t
                ::default_debugger_interactive_commands_print[] =
                        CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_PRINT;

// see declaration
const char *crash_handler_platform_specific_t
                ::default_debugger_interactive_commands_quit[] =
                        CRASH_WIN_DEBUGGER_DEFAULT_INTERACTIVE_COMMANDS_QUIT;

// see declaration
void crash_handler_platform_specific_t::free_and_null_if_valid(void **data)
{
    void *actual_data = *data;
    if (actual_data)
    {
        free(actual_data);
        actual_data = 0;
    }
}

#ifdef CRASH_WIN_HAVE_SET_INVALID_PARAMETER_HANDLER
// see declaration
void crash_handler_platform_specific_t::_invalid_parameter_handler_crash(
        const wchar_t * expression, const wchar_t * function,
        const wchar_t * file, unsigned int line, uintptr_t pReserved)
{
    // Variables used to generate a crash signal
    int x = 0;
    int y = 0;

    // Print information about the error if it is provided.
    if (expression && function && file)
    {
        // This is not a signal handler, so it is safe to use non-reentrant
        // functions.
        fprintf(stderr, "Windows invalid parameter detected:\n");
        fprintf(stderr,
# ifdef __MINGW64_VERSION_MAJOR
                // C99-style
                "  \"%ls\" in %ls at %ls:%u\n",
# else
                // MSVC-style
                "  \"%S\" in %S at %S:%u\n",
# endif
                expression, function, file, line);
    }

    // Crash on purpose (invoke the crash signal handler)
    x = x / y;
}
#endif

#endif // CRASH_WIN_ENABLED
