/**
 * @file
 * @brief Operating system-independent crash handling routines.
 *
 * Implements OS-independent crash handling routines declared in crash.h.
 *
 * Additionally, defines necessary stubs in the case that
 * CRASH_IMPLEMENTATION_SPECIFIC_SUPPORTED is not defined in crash.h.
**/

#include "AppHdr.h"

#include "crash.h"

#include "dbg-asrt.h"
#include "syscalls.h"

#include <iterator>
#include <signal.h>
#ifndef TARGET_COMPILER_VC
# include <sys/time.h>
#else
# include <time.h>
#endif

// Check if failsafe implementations are necessary.
#if defined(CRASH_DISABLE_OS_IMPLEMENTATION) || defined(DOXYGEN_ONLY)
/**
 * @brief
 *  If defined, this file provides a minimal crash_handler_t implementation.
 */
# define CRASH_NEED_FAILSAFE_IMPLEMENTATION
#endif

// see declaration
void abstract_crash_handler_t::_crash_signal_handler(int sig_num)
{
    // Call the actual handler.
    // If the crash_handler instance has not yet been allocated, it is expected
    // that this function will fail.
    crash_handler_t::instance()->handle_signal(sig_num);
}

// see declaration
abstract_crash_handler_t::abstract_crash_handler_t()
        : m_original_signal_handlers()
{
    // Initialize remaining member variables
    m_crash_signal = 0;
    m_current_depth = 0;

    // Init the crash mutex
    mutex_init(m_crash_mutex);

    // Register signal handler with crash-level signals now that necessary
    // member variables have been initialized.
    init_crash_signal_handler();
}

// see declaration
abstract_crash_handler_t::~abstract_crash_handler_t()
{
    // Restore original handlers to the crash-level signals.
    restore_original_signal_handlers();

    // Clean up the mutex
    mutex_destroy(m_crash_mutex);
}

// see declaration
void abstract_crash_handler_t::disable_other_crashes()
{
    // If one thread calls end() without going through a crash (a handled
    // fatal error), no one else should be allowed to crash.  We're already
    // going down so blocking the other thread is ok.
    mutex_lock(m_crash_mutex);
}

/**
 * @internal
 * @par Unix Signals
 *  If unix signals are available, a human-readable error message will be
 *  printed along with the signal number.
 */
void abstract_crash_handler_t::dump_crash_info(FILE* file)
{
    const char *name = NULL;
# ifdef UNIX
    // Look up a string corresponding to the signal
    name = strsignal(m_crash_signal);
    if (name == NULL)
        name = "INVALID";
# endif
    fprintf(file, "Crash caused by signal #%d", m_crash_signal);
    if (name)
        fprintf(file, ": %s", name);
    fprintf(file, "\n\n");
}

/**
 * @internal
 * Warning: This handler calls some unsafe functions for a signal handler.
 * <br>
 * This handler is mainly used as a "best effort" to get some just-in-time
 * debugging information for crashes such as assertion failures, null pointer
 * dereference, division by zero, etc.  More severe crashes, such as malloc
 * failures, will likely result in this signal handler failing spectacularly.
 * <br>
 * For these severe crashes, it is recommended to instead run a debugger
 * interactively, placing a breakpoint at the beginning of this signal handler
 * (or better yet, the corresponding C wrapper) in order to preserve the program
 * state.
 */
void abstract_crash_handler_t::handle_signal(int sig_num)
{
    // We rely on mutexes ignoring locks held by the same thread.
    // On some platforms, this must be explicitly enabled (which we do).

    // This mutex is never unlocked again -- the first thread to crash will
    // do a dump then terminate the process while everyone else waits here
    // forever.

    // XXX: This is a bit dangerous: if we catch a signal while any
    // non-asynch-signal-safe function is executing, and then call
    // pthread_mutex_lock() (which is also not asynch-signal-safe),
    // the behaviour is undefined.
    mutex_lock(m_crash_mutex);

    // Update the member variables
    m_current_depth++;
    m_crash_signal = sig_num;

    // During a crash, we may be in an inconsistent state (duh).  Doing a number
    // of things can cause a lock up, especially calling non-reentrant functions
    // like malloc() and friends, used by C++ basics like std::string
    // internally.
    // There's no reliable way to ensure such things won't happen.  A pragmatic
    // solution is to abort the crash dump.
    alarm(120);

# ifdef WATCHDOG
    /* Infinite loop protection.

       Not tickling the watchdog for 60 seconds of user CPU time (not wall
       time!) means something is terribly wrong.  Even worst hogs like
       pre-0.6 god renouncement or current Time Step in the Abyss don't take
       more than several seconds.

       DGL only -- local players will notice the game is stuck and be able
       to kill it.

       It's likely to die horribly -- it's one of signals that is often
       received while in non-signal safe functions, especially malloc()
       which _will_ fuck the process up (remember, C++ can't blink without
       malloc()ing something).  In such cases, alarm() above will kill us.
       That's nasty and random, but at least should give us backtraces most
       of the time, and avoid dragging down the servers.  And even if for
       some odd reason SIGALRM won't kill us, the worst that can happen is
       wasting 100% CPU which is precisely what happens right now.
    */
    if (sig_num == SIGVTALRM)
        die_noline("Stuck game with 100%% CPU use\n");
# endif

    // Attempt to write out a crash log
    do_crash_dump(m_current_depth);

    // Do not bother decrementing m_current_depth, as the following calls will
    // not return.

    // Now crash for real.
    signal(sig_num, SIG_DFL);
    raise(sig_num);
}

/**
 * @internal
 * @par POSIX
 *  If POSIX signals are available, this function will attempt to register all
 *  signals [1,64] as crash signals if they are not specifically blacklisted.
 */
void abstract_crash_handler_t::init_crash_signal_handler()
{
    // Used for iterating through the whitelist (and blacklist if enabled)
    int i;
    // Holds the previous signal handler function pointer.
    std::pair<int, void (*)(int)> previous_handler_pair;

    // ANSI C signals -- use a whitelist
    // Known signals which *ARE* considered a crash and will be handled.
    //  Note: The whitelist takes precedence over any blacklist entries
    int signal_whitelist[] =
    {
#ifdef SIGABRT
        SIGABRT,
#endif
#ifdef SIGFPE
        SIGFPE,
#endif
#ifdef SIGILL
        SIGILL,
#endif
#ifdef SIGSEGV
        SIGSEGV,
#endif
#ifdef SIGTERM
        SIGTERM,
#endif
    };
    // The number of elements in the whitelist array
    int signal_whitelist_num_elements = sizeof(signal_whitelist) / sizeof(int);

#ifdef USE_UNIX_SIGNALS
    // Create a signal blacklist if using POSIX signals
    // Used to iterate through the additional POSIX signals
    int j;
    // Known signals which are not considered a crash (i.e., no crash handler).
    int signal_blacklist[] =
    {
# ifdef SIGALRM
        SIGALRM,
# endif
# ifdef SIGHUP
        SIGHUP,
# endif
# ifdef SIGQUIT
        SIGQUIT,
# endif
# ifdef SIGINT
        SIGINT,
# endif
# ifdef SIGCHLD
        SIGCHLD,
# endif
# ifdef SIGTSTP
        SIGTSTP,
# endif
# ifdef SIGCONT
        SIGCONT,
# endif
# ifdef SIGIO
        SIGIO,
# endif
# ifdef SIGPROF
        SIGPROF,
# endif
# ifdef SIGTTOU
        SIGTTOU,
# endif
# ifdef SIGTTIN
        SIGTTIN,
# endif
# ifdef SIGKILL
        SIGKILL,
# endif
# ifdef SIGSTOP
        SIGSTOP,
# endif
# ifdef SIGWINCH
        SIGWINCH,
# endif
    };
    // The number of elements in the blacklist array
    int signal_blacklist_num_elements = sizeof(signal_blacklist) / sizeof(int);
#endif

    // Assign the crash signal handler to all signals in the whitelist
    //  This also covers the unlikely case that the whitelisted signals are not
    //  in the blacklisted signal range check.
    for (i = 0; i < signal_whitelist_num_elements; i++)
    {
        previous_handler_pair.first = signal_whitelist[i];
        previous_handler_pair.second = signal(signal_whitelist[i],
                _crash_signal_handler);
        m_original_signal_handlers.insert(previous_handler_pair);
    }

    // Only attempt to register non-whitelisted signal handlers if the target
    // is known to support POSIX signal extensions.
#ifdef USE_UNIX_SIGNALS
    // POSIX signals -- use a blacklist
    for (j = 1; j <= 64; j++)
    {
        // Check if this signal is within the blacklist array
        for (i = 0; i < signal_blacklist_num_elements; i++)
        {
            if (j == signal_blacklist[i])
                break;
        }
        if (i == signal_blacklist_num_elements)
        {
            // Not blacklisted, add a crash signal handler
            previous_handler_pair.first = j;
            previous_handler_pair.second = signal(j, _crash_signal_handler);
            // It is fine if this signal was already registered via the
            // whitelist;  std::map will reject the duplicate key insert.
            m_original_signal_handlers.insert(previous_handler_pair);
        }
    }
#endif
}

// see declaration
void abstract_crash_handler_t::restore_original_signal_handlers()
{
    // Iterator for the map
    std::map<int, void (*)(int)>::iterator ocsh_iter;

    // Iterate through the map, restoring the signal handlers
    for (ocsh_iter = m_original_signal_handlers.begin();
            ocsh_iter != m_original_signal_handlers.end(); ocsh_iter++)
    {
        signal(ocsh_iter->first, ocsh_iter->second);
    }

    // Purge the map of old data
    m_original_signal_handlers.clear();
}

/**
 * @internal
 * @par Windows
 *  This function uses real time instead of CPU time.
 */
void abstract_crash_handler_t::watchdog()
{
#ifdef UNIX
    struct itimerval t;
    t.it_interval.tv_sec = 0;
    t.it_interval.tv_usec = 0;
    t.it_value.tv_sec = 60;
    t.it_value.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, &t, 0);
#else
    // Real time rather than CPU time.
    // This will break DGL, but it makes no sense on Windows anyway.
    // Mapstat is cool with this.
    alarm(60);
#endif
}

// see declaration
void abstract_crash_handler_t::write_no_call_gdb_available(FILE* file)
{
    fprintf(file, "JIT gdb execution unsupported on this platform.\n");
}

// see declaration
void abstract_crash_handler_t::write_no_stack_trace_available(FILE* file)
{
    fprintf(file, "Unable to get stack trace on this platform.\n");
}

// see declaration
void crash_handler_t::destroy_instance()
{
    // TODO: Thread safety
    if (sm_instance)
    {
        delete sm_instance;
        sm_instance = 0;
    }
}

// see declaration
crash_handler_t * crash_handler_t::instance()
{
    // TODO: Thread safety
    if (sm_instance == 0)
    {
        // No instance yet, create one.
        sm_instance = new crash_handler_t();
    }
    return sm_instance;
}

// see declaration
crash_handler_t *crash_handler_t::sm_instance = 0;

// See if failsafe os-specific debugging functions must be defined.
#ifdef CRASH_NEED_FAILSAFE_IMPLEMENTATION

/**
 * @internal
 * @par Unsupported OS
 * Indicates that call_gdb are unsupported for the platform.
 */
void crash_handler_t::call_gdb(FILE* file)
{
    write_no_call_gdb_available(file);
}

/**
 * @internal
 * @par Unsupported OS
 * Performs the basic initialization in abstract_crash_handler_t's constructor.
 */
crash_handler_t::crash_handler_t()
        : abstract_crash_handler_t()
{
    // Not used
    m_platform_specifics = 0;
}

/**
 * @internal
 * @par Unsupported OS
 * Performs the basic cleanup present in abstract_crash_handler_t's destructor.
 */
crash_handler_t::~crash_handler_t()
{
    // Base destructor called automatically -- nothing left to do.
}

/**
 * @internal
 * @par Unsupported OS
 * Note the lack of stack trace support for the current target in file.
 */
void crash_handler_t::write_stack_trace(FILE* file, int ignore_count)
{
    write_no_stack_trace_available(file);
}

#endif // _CRASH_NEED_FAILSAFE_IMPLEMENTATION
