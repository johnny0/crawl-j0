/**
 * @file
 * @brief Assertions and crashing.
 *
 * Defines functions used to handle assertions and crashes tied specifically to
 * crawl's logic and state (in contrast to crash.h's more generic
 * functionality).
**/

#ifndef DBG_ASRT_H
#define DBG_ASRT_H

/**
 * @brief Perform a full crash dump for the current game.
 *
 * This function takes care of opening a crashlog file, calling any desired
 * functions to dump information, and closing the crashlog as cleanly as
 * possible.  The location of this crashlog is relayed to the user, but the
 * method of delivering this information varies based on the target system.
 *
 * @param current_depth
 *  The current crash depth.
 *  A crash depth of two of greater indicates a recursive crash.
 */
void do_crash_dump(unsigned int current_depth);

#endif /* DBG_ASRT_H */
