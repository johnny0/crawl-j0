/**
 * @file
 * @brief Implements functions declared in crash.h for POSIX systems.
 *
 * Handles crash-related functions on POSIX systems, using standard debugging
 * functions when possible.
**/

#include "AppHdr.h"

#include "crash.h"

#include "state.h"

// Only define if this is a UNIX-type system.
// Note that Cygwin uses the Windows API implementation.
#if !defined(CRASH_DISABLE_OS_IMPLEMENTATION) \
    && ( defined(UNIX) \
            && !(defined(TARGET_COMPILER_CYGWIN) \
                    || defined(TARGET_OS_WINDOWS)) \
    ) \
    || defined(DOXYGEN_ONLY)
/**
 * @brief If defined, provides a POSIX crash_handler_t implementation.
 *
 * If not defined, this file's, the implementation will not be compiled.
 */
# define CRASH_POSIX_ENABLED
#endif

// Only compile for POSIX targets.
# ifdef CRASH_POSIX_ENABLED

# include <errno.h>
# include <sys/wait.h>
# include <unistd.h>

// Determine backtrace support.
# if !( defined(TARGET_CPU_MIPS) || \
        defined(TARGET_OS_FREEBSD) || \
        defined(TARGET_OS_NETBSD) || \
        defined(TARGET_OS_OPENBSD) || \
        defined(__ANDROID__) )

// Supported UNIX platform for backtrace
#  define CRASH_POSIX_BACKTRACE_SUPPORTED

// Determine if dynamic loading is needed.
#  ifdef TARGET_OS_MACOSX
#   define CRASH_POSIX_NEED_DYNAMIC_BACKTRACE
#  endif

// Includes for stack traces
#  include <cxxabi.h>
#  ifdef CRASH_POSIX_NEED_DYNAMIC_BACKTRACE
#   include <dlfcn.h>
#  else
#   include <execinfo.h>
#  endif

# endif

// Define necessary types and templates for dynamic loading of backtrace.
# ifdef CRASH_POSIX_NEED_DYNAMIC_BACKTRACE

/** @brief Function pointer type for backtrace function */
typedef int (*backtrace_t)(void * *, int);
/** @brief Function pointer type for backtrace_symbols function  */
typedef char **(*backtrace_symbols_t)(void * const *, int);

/**
 * @brief
 *  Used to convert from void* to function pointer (without a compiler warning).
 *
 * @todo Finish documenting nasty_cast template.
 *
 * @tparam TO
 *
 * @tparam FROM
 *
 * @param f
 */
template <typename TO, typename FROM> TO nasty_cast(FROM f)
{
    union
    {
        FROM f;
        TO t;
    } u;

    u.f = f;

    return u.t;
}
# endif // CRASH_POSIX_NEED_DYNAMIC_BACKTRACE

/**
 * @par POSIX
 * Call GDB using standard POSIX fork/dup/exec functions.
 */
void crash_handler_t::call_gdb(FILE *file)
{
    if (crawl_state.no_gdb)
        return (void)fprintf(file, "%s\n", crawl_state.no_gdb);

    fprintf(file, "Trying to run gdb.\n");
    fflush(file); // so we can use fileno()

    char attach_cmd[20] = {};
    snprintf(attach_cmd, sizeof(attach_cmd), "attach %d", getpid());

    switch (int gdb = fork())
    {
    case -1:
        return (void)fprintf(file, "Couldn't fork: %s\n", strerror(errno));
    case 0:
        {
            int fd = fileno(file);
            dup2(fd, 1);
            dup2(fd, 2);
            close(fd);

            const char* argv[] =
            {
                "gdb",
                "-batch",
                "-ex", "show version", // Too bad -iex needs gdb >=7.5 (jessie)
                "-ex", attach_cmd,
                "-ex", "thread apply all bt full",
                0
            };
            execv("/usr/bin/gdb", (char* const*)argv);
            printf("Failed to start gdb: %s\n", strerror(errno));
            fflush(stdout);
            _exit(0);
        }
        return;
    default:
        waitpid(gdb, 0, 0);
    }
}

/**
 * @par POSIX
 * Initialize the crash handler as necessary for POSIX systems.
 */
crash_handler_t::crash_handler_t()
        : abstract_crash_handler_t()
{
    // Nothing extra is needed.
    m_platform_specifics = 0;
}

/**
 * @par POSIX
 * Perform any necessary cleanup for the POSIX crash handler.
 */
crash_handler_t::~crash_handler_t()
{
    // Base destructor called automatically -- nothing left to do.
}

/**
 * @par POSIX
 * Perform a stack trace for POSIX systems using backtrace.
 * In the case of an unsupported POSIX platform, print an appropriate warning
 * to file the passed file.
 */
void crash_handler_t::write_stack_trace(FILE* file, int ignore_count)
{
# ifdef CRASH_POSIX_BACKTRACE_SUPPORTED
    // Backtrace supported
    void* frames[50];

#  if defined(CRASH_POSIX_NEED_DYNAMIC_BACKTRACE)
    backtrace_t backtrace;
    backtrace_symbols_t backtrace_symbols;
    backtrace = nasty_cast<backtrace_t, void*>(dlsym(RTLD_DEFAULT, "backtrace"));
    backtrace_symbols = nasty_cast<backtrace_symbols_t, void*>(dlsym(RTLD_DEFAULT, "backtrace_symbols"));
    if (!backtrace || !backtrace_symbols)
    {
        fprintf(file, "Couldn't get a stack trace.\n");
        return;
    }
#  endif

    int num_frames = backtrace(frames, ARRAYSZ(frames));
    char **symbols = backtrace_symbols(frames, num_frames);

#  if !defined(CRASH_POSIX_NEED_DYNAMIC_BACKTRACE)
    if (symbols == NULL)
    {
        fprintf(file, "Out of memory.\n");

        // backtrace_symbols_fd() can print out the stack trace even if
        // malloc() can't find any free memory.
        backtrace_symbols_fd(frames, num_frames, fileno(file));
        return;
    }
#  endif

    fprintf(file, "Obtained %d stack frames.\n", num_frames);

    // Now we prettify the printout to even show demangled C++ function names.
    string bt = "";
    for (int i = 0; i < num_frames; i++)
    {
#  if defined(TARGET_OS_MACOSX)
        // OSX-specific backtrace results
        char *addr = ::strstr(symbols[i], "0x");
        char *mangled = ::strchr(addr, ' ') + 1;
        char *offset = ::strchr(addr, '+');
        char *postmangle = ::strchr(mangled, ' ');
        if (mangled)
            *(mangled - 1) = 0;
        bt += addr;
        int status;
        bt += ": ";
        if (addr && mangled)
        {
            if (postmangle)
                *postmangle = '\0';
            char *realname = abi::__cxa_demangle(mangled, 0, 0, &status);
            if (realname)
                bt += realname;
            else
                bt += mangled;
            bt += " ";
            bt += offset;
            free(realname);
        }
#  else // TARGET_OS_MACOSX
        // Standard UNIX backtrace results
        bt += symbols[i];
        int status;
        // Extract the identifier from symbols[i].  It's inside of parens.
        char *firstparen = ::strchr(symbols[i], '(');
        char *lastparen = ::strchr(symbols[i], '+');
        if (firstparen != 0 && lastparen != 0 && firstparen < lastparen)
        {
            bt += ": ";
            *lastparen = '\0';
            char *realname = abi::__cxa_demangle(firstparen + 1, 0, 0, &status);
            if (realname != NULL)
                bt += realname;
            free(realname);
        }
#  endif
        bt += "\n";
    }

    fprintf(file, "%s", bt.c_str());

    free(symbols);
# else // CRASH_POSIX_BACKTRACE_SUPPORTED
    // Backtrace unavailable - write out the generic unsupported message
    write_no_stack_trace_available(file);
# endif
}

#endif // CRASH_POSIX_ENABLED
