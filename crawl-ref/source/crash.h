/**
 * @file
 * @brief Declares a crash handling API for crawl.
 *
 * Functions which are expected to be especially OS-dependent are noted as such
 * with a Doxygen 'note' tag.
 **/

#ifndef CRASH_H
#define CRASH_H

#include "threads.h"

#include <map>
#include <stdio.h>

#ifdef DOXYGEN_ONLY
/**
 * @brief Define this macro to manually disable compilation of any OS-specific
 *  crash.h implementation.
 */
# define CRASH_DISABLE_OS_IMPLEMENTATION
#endif

/**
 * @brief An abstract class which declares a crash handling API for crawl.
 *
 * Contains methods for printing a stack trace and calling a debugger
 * (in order to obtain a more detailed stack trace).
 *
 * An crash handler will, by default, assign a C signal handler to all signals
 * which indicate a crash is occurring in the program.  This signal assignment
 * is during object construction.
 *
 * Constructors are declared protected in anticipation of a Singleton pattern
 * being used by a child class (as most of this lower-level initialization
 * should only be performed once).
 */
class abstract_crash_handler_t
{
public:
    /**
     * @brief Call gdb to print an extended backtrace to the passed file.
     *
     * If gdb is not available or otherwise cannot be invoked, a relevant
     * warning will be written to the file instead.
     *
     * @param file
     *  The stream to write the gdb output to.
     *
     * @note Platform-specific.
     */
    virtual void call_gdb(FILE *file) = 0;

    /**
     * @brief Disable the handling of all other crashes.
     *
     * Disable all other crashes from being handled by intentionally deadlocking
     * any would-be crashing threads.  This is typically called when a thread
     * has handled a crash and is preparing to exit the game.
     */
    void disable_other_crashes();

    /**
     * @brief Dump crash information to the passed file.
     *
     * @param file
     *  The output stream to write the crash information to.
     */
    void dump_crash_info(FILE* file);

    /**
     * @brief The handler to be invoked when a crash-level signal is received.
     *
     * @param sig_num
     *  The signal to handle.
     */
    void handle_signal(int sig_num);

    /**
     * @brief Kick off or reset/refresh a watchdog/heartbeat timer.
     *
     * This is mainly used to help the program detect deadlock or an infinite
     * loop. If this function is not called periodically, the timer will trigger
     * and fire off an alarm signal.
     */
    void watchdog();

    /**
     * @brief Write a stack trace to the passed file.
     *
     * This is very much os-specific, so stack traces may differ slightly from
     * system to system.  If a stack trace cannot be obtained, a relevant
     * warning will instead be written to file.
     *
     * @param file
     *  The stream to write the stack trace to.
     * @param ignore_count
     *  The number of stack frames to skip before printing the stack.
     * @todo Verify that the documentation of ignore_count is accurate.
     *
     * @note Platform-specific.
     */
    virtual void write_stack_trace(FILE* file, int ignore_count) = 0;

protected:
    /**
     * @brief Basic destructor for an abstract_crash_handler_t.
     *
     * Attempt to clean up resource initialization and signal handlers
     * registration performed during object construction.
     */
    virtual ~abstract_crash_handler_t();

    /**
     * @brief Basic constructor.
     *
     * Performs basic initialization of member variables and registers a C
     * signal handler for crash-level signals.
     */
    abstract_crash_handler_t();

    /**
     * @brief
     *  Assigns the C signal handler for crash-severity signals.
     */
    void init_crash_signal_handler();

    /**
     * @brief
     *  Restores the original handlers displaced by init_crash_signal_handler().
     */
    void restore_original_signal_handlers();

    /**
     * @brief Indicate that just-in-time gdb is unavailable for the current
     *  platform.
     *
     * @param file
     *   The stream to write the warning to.
     */
    void write_no_call_gdb_available(FILE* file);

    /**
     * @brief Indicate stack traces are unavailable for the current platform.
     *
     * @param file
     *   The stream to write the warning to.
     */
    void write_no_stack_trace_available(FILE* file);

private:
    /**
     * @brief A wrapper function for handling crash-level signals.
     *
     * Wraps the underlying C++ implementation in a form the C signal API
     * understands.  Note that using C++-only functionality from a C signal
     * handler has implementation defined behavior (but typically works provided
     * things are kept simple).
     */
    static void _crash_signal_handler(int sig_num);

    /**
     * @brief The mutex used by the to limit crash dumps to one at a time.
     *
     * Note that using a mutex in a signal handler is inherently unsafe.
     * This is used as a "best effort" approach.
     *
     * @todo Use an atomic variable instead, if available.
     */
    mutex_t m_crash_mutex;

    /**
     * @brief The last signal number to cause the signal handler to be invoked.
     */
    int m_crash_signal;

    /**
     * @brief Stores the original signal handlers for crash-level signals.
     *
     * The map's pairs have signals for keys and the original signal handler
     * function pointers as values.
     */
    std::map<int, void (*)(int)> m_original_signal_handlers;

    /**
     * @brief The current depth of the signal handler.
     *
     * The depth indicates the level of recursion in the signal handler.
     * Zero indicates there is no current signal being handled.
     * A depth of one indicates a signal is being handled, but without any
     * recursion.  Any depth greater than one indicates recursive crash
     * behavior (i.e., the handler itself is triggering a crash).
     */
    unsigned int m_current_depth;
};

/**
 * Forward declaration of platform-specific implementation details of the
 * crash-handler.
 */
class crash_handler_platform_specific_t;

/**
 * A platform-specific implementation of an abstract crash handler.
 *
 * @note All members are platform-specific.
 */
class crash_handler_t : public abstract_crash_handler_t
{
public:
    /** @see abstract_crash_handler_t::call_gdb */
    void call_gdb(FILE *file);

    /**
     * @brief Destroys the Singleton instance of this class.
     *
     * This is typically used during the program's shutdown / restart sequence
     * to release all dynamically-allocated and initialized resources.
     *
     * Care must be taken not to destroy the instance when it may be in use by
     * other areas of the code.
     */
    static void destroy_instance();

    /**
     * @brief Returns the Singleton instance of this class.
     *
     * If an instance does not yet exist one will be allocated.  Otherwise,
     * the existing Singleton instance will be returned.
     *
     * @return
     *  The Singleton instance of this class.
     */
    static crash_handler_t * instance();

    /** @see abstract_crash_handler_t::write_stack_trace */
    void write_stack_trace(FILE* file, int ignore_count);

protected:
    /**
     * @brief Platform-specific crash handler destructor.
     *
     * @see abstract_crash_handler_t::~abstract_crash_handler_t
     *
     * @note Platform-specific.
     */
    ~crash_handler_t();

    /**
     * @brief Platform-specific crash handler constructor.
     *
     * @see abstract_crash_handler_t::abstract_crash_handler_t
     *
     * @note Platform-specific.
     */
    crash_handler_t();

private:
    /**
     * An object containing necessary platform-specific data and methods.
     */
    crash_handler_platform_specific_t *m_platform_specifics;

    /**
     * The Singleton instance of this class.
     */
    static crash_handler_t *sm_instance;
};

#endif
